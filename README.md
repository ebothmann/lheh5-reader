# LHEF HDF5 Reader


## Purpose

This is a demonstration program for the processing of LesHouches Event Files in HDF5 format, as described in [arXiv:1905.05120](https://arxiv.org/abs/1905.05120)

## Installation

To install the reader, run

```
git clone https://gitlab.com/shoeche/lheh5-reader.git
cd lheh5-reader
mkdir build
cd build
cmake ..
make
```

## Test

To test the reader, run

```
./reader ../test.hdf5
```

You should see the following output

```
cross section = 1617.04 +- 113.186
```
