#include "hdf5_event_reader.h"
#define USING__MPI
#ifdef USING__MPI
#include "mpi.h"
#endif

int main(int argc,char **argv)
{
  if (argc<2) return -1;
  // create file and read events
  std::string fname=argv[1];
  HighFive::FileAccessProps fprops;
#ifdef USING__MPI
  MPI_Init(&argc,&argv);
  fprops.add(HighFive::MPIOFileAccess(MPI_COMM_WORLD,MPI_INFO_NULL));
  fprops.add(HighFive::MPIOCollectiveMetadata());
#endif
  HighFive::File file(fname,HighFive::File::ReadOnly,fprops);
  LHEH5::LHEFile *e(new LHEH5::LHEFile());
  e->ReadHeader(file);
  double totalxs=e->TotalXS();
  long int nevts=file.getDataSet("events").
    getSpace().getDimensions().front();
#ifndef USING__MPI
  int size=1, rank=0;
#else
  int size=MPI::COMM_WORLD.Get_size();
  int rank=MPI::COMM_WORLD.Get_rank();
  MPI_Bcast(&nevts,1,MPI_LONG_INT,0,MPI_COMM_WORLD);
  if (rank==0)
#endif
  std::cout<<"File '"<<fname<<"' -> "<<std::flush;
  size_t iStart=rank*nevts/size;
  size_t iStop=(rank+1)*nevts/size-1;
  if (rank==size-1) iStop=nevts-1;
  e->ReadEvents(file,iStart,iStop-iStart+1);
  // process events (print and calculate cross section)
  double sum[5]={0,0,0,0,0};
  for (size_t i(0);i<e->NEvents();++i) {
    LHEH5::Event evt(e->GetEvent(i));
    if (rank==0 && argc>3) std::cout<<evt<<"\n";
    sum[0]+=evt.trials;
    sum[1]+=evt.wgts[0];
    sum[2]+=evt.wgts[0]*evt.wgts[0];
    sum[3]+=(evt.wgts[0]?1:0);
    sum[4]=std::max(sum[4],std::abs(evt.wgts[0]));
  }
#ifdef USING__MPI
  MPI_Allreduce(MPI_IN_PLACE,sum,4,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE,&sum[4],1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
  if (rank==0)
#endif
  std::cout<<"XS = "<<sum[1]/sum[0]<<" +- "
	   <<sqrt((sum[2]/sum[0]-pow(sum[1]/sum[0],2))/(sum[0]-1))
	   <<" pb, w_max = "<<sum[4]*sum[3]/sum[1]<<"\n";
  if (argc>2) {
    double max(atof(argv[2])*e->GetEvent(0).pinfo.xsec*sum[0]/sum[3]);
    for (size_t i(0);i<e->NEvents();++i) {
      LHEH5::Event evt(e->GetEvent(i));
      if (argc>2 && std::abs(evt.wgts[0])>max)
	std::cout<<"Event "<<i<<": "
		 <<evt.wgts[0]*sum[3]/sum[1]<<"\n"
		 <<evt<<"\n";
    }
  }
  delete e;
#ifdef USING__MPI
  MPI_Finalize();
#endif
  return 0;
}
